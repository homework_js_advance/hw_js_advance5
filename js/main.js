"use strict"

class Card {
  constructor(user, post) {
      this.user = user;
      this.post = post;
      this.element = this.createCardElement();
      this.setupDeleteButton();
      this.setupEditButton();
  }

  createCardElement() {
      const postElement = document.createElement('div');
      postElement.className = 'post';

      const userElement = document.createElement('div');
      userElement.className = 'user';
      userElement.textContent = `User: ${this.user.name} ${this.user.surname} (${this.user.email})`;

      const postContentElement = document.createElement('div');
      postContentElement.className = 'content';

      const postTitleElement = document.createElement('div');
      postTitleElement.className = 'title';
      postTitleElement.textContent = this.post.title;

      const postTextElement = document.createElement('div');
      postTextElement.className = 'text';
      postTextElement.textContent = this.post.body;

      postContentElement.appendChild(postTitleElement);
      postContentElement.appendChild(postTextElement);

      postElement.appendChild(userElement);
      postElement.appendChild(postContentElement);

      return postElement;
  }

  setupDeleteButton() {
      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Delete';
      deleteButton.addEventListener('click', () => this.handleDelete());
      this.element.appendChild(deleteButton);
  }

  setupEditButton() {
      const editButton = document.createElement('button');
      editButton.textContent = 'Edit';
      editButton.addEventListener('click', () => this.handleEdit());
      this.element.appendChild(editButton);
  }

  handleDelete() {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
          method: 'DELETE',
      })
          .then(response => {
              if (response.ok) {
                  this.element.remove();
              } else if (response.status === 404) {
                  console.warn('Post not found:', this.post.id);
              } else {
                  console.error('Error deleting post:', response.status);
              }
          })
          .catch(error => console.error('Error deleting post:', error));
  }

  handleEdit() {
      const newTitle = prompt('Enter the new title:', this.post.title);
      const newText = prompt('Enter the new text:', this.post.body);

      if (newTitle !== null && newText !== null) {
          this.post.title = newTitle;
          this.post.body = newText;

          fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
              method: 'PUT',
              headers: {
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  title: newTitle,
                  body: newText,
              }),
          })
              .then(response => {
                  if (!response.ok) {
                      console.error('Error updating post:', response.status);
                  }
              })
              .catch(error => console.error('Error updating post:', error));

          this.updateCardContent();
      }
  }

  updateCardContent() {
      const titleElement = this.element.querySelector('.title');
      const textElement = this.element.querySelector('.text');

      titleElement.textContent = this.post.title;
      textElement.textContent = this.post.body;
  }
}

document.addEventListener("DOMContentLoaded", function () {
  showLoadingAnimation();

  // таймаут лише для того, щоб роздивитися яка гарна кастомна аніміція 
  setTimeout(() => {
      fetch('https://ajax.test-danit.com/api/json/users')
          .then(response => response.json())
          .then(users => {
              fetch('https://ajax.test-danit.com/api/json/posts')
                  .then(response => response.json())
                  .then(posts => {
                      const combinedData = posts.map(post => {
                          const user = users.find(u => u.id === post.userId);
                          return { user, post };
                      });

                      hideLoadingAnimation();
                      showAddButton();
                      displayPosts(combinedData);
                  })
                  .catch(error => {
                      console.error('Error fetching posts:', error);
                      hideLoadingAnimation();
                  });
          })
          .catch(error => {
              console.error('Error fetching users:', error);
              hideLoadingAnimation();
          });
  }, 1500); // Затримка завантаження на 1.5 секунди
});

function showLoadingAnimation() {
  const loadingAnimation = document.getElementById('loading-animation');
  loadingAnimation.style.display = 'block';
}

function hideLoadingAnimation() {
  const loadingAnimation = document.getElementById('loading-animation');
  const twitterFeed = document.getElementById('twitter-feed');

  loadingAnimation.style.display = 'none';
  twitterFeed.classList.add('loaded');
}

function showAddButton() {
  const addButton = document.getElementById('add-button');
  addButton.style.display = 'block';
}

function openModal() {
  const modal = document.getElementById('modal');
  modal.style.display = 'block';
}

function closeModal() {
  const modal = document.getElementById('modal');
  modal.style.display = 'none';
}

function displayPosts(data) {
  const twitterFeed = document.getElementById('twitter-feed');

  twitterFeed.innerHTML = '';

  data.forEach(item => {
      const card = new Card(item.user, item.post);
      twitterFeed.appendChild(card.element);
  });
}

function addPost() {
  const titleInput = document.getElementById('post-title');
  const textInput = document.getElementById('post-text');

  const title = titleInput.value;
  const text = textInput.value;

  if (title && text) {
      fetch('https://ajax.test-danit.com/api/json/posts', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              title: title,
              body: text,
              userId: 1,
          }),
      })
          .then(response => response.json())
          .then(newPost => {
              closeModal();

              const twitterFeed = document.getElementById('twitter-feed');
              const card = new Card({ name: 'Андрій', surname: 'Мацевко', email: 'Zevko@example.com' }, newPost);
              twitterFeed.insertBefore(card.element, twitterFeed.firstChild);
          })
          .catch(error => console.error('Error adding post:', error));
  }
}